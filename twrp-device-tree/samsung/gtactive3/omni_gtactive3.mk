#
# Copyright (C) 2023 The Android Open Source Project
# Copyright (C) 2023 SebaUbuntu's TWRP device tree generator
#
# SPDX-License-Identifier: Apache-2.0
#

# Inherit from those products. Most specific first.
$(call inherit-product, $(SRC_TARGET_DIR)/product/core_64_bit.mk)
$(call inherit-product, $(SRC_TARGET_DIR)/product/full_base_telephony.mk)

# Inherit some common Omni stuff.
$(call inherit-product, vendor/omni/config/common.mk)

# Inherit from gtactive3 device
$(call inherit-product, device/samsung/gtactive3/device.mk)

PRODUCT_DEVICE := gtactive3
PRODUCT_NAME := omni_gtactive3
PRODUCT_BRAND := samsung
PRODUCT_MODEL := SM-T577U
PRODUCT_MANUFACTURER := samsung

PRODUCT_GMS_CLIENTID_BASE := android-samsung-ss

PRODUCT_BUILD_PROP_OVERRIDES += \
    PRIVATE_BUILD_DESC="gtactive3ue-user 13 TP1A.220624.014 T577UUEU4EWC2 release-keys"

BUILD_FINGERPRINT := samsung/gtactive3ue/gtactive3:13/TP1A.220624.014/T577UUEU4EWC2:user/release-keys
