#
# Copyright (C) 2023 The Android Open Source Project
# Copyright (C) 2023 SebaUbuntu's TWRP device tree generator
#
# SPDX-License-Identifier: Apache-2.0
#

PRODUCT_MAKEFILES := \
    $(LOCAL_DIR)/omni_gtactive3.mk

COMMON_LUNCH_CHOICES := \
    omni_gtactive3-user \
    omni_gtactive3-userdebug \
    omni_gtactive3-eng
