#
# Copyright (C) 2023 The LineageOS Project
#
# SPDX-License-Identifier: Apache-2.0
#

PRODUCT_MAKEFILES := \
    $(LOCAL_DIR)/lineage_gtactive3.mk

COMMON_LUNCH_CHOICES := \
    lineage_gtactive3-user \
    lineage_gtactive3-userdebug \
    lineage_gtactive3-eng
